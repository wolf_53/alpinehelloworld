image: docker:latest

stages:
  - Build
  - Scan docker image
  - Test Unitaire
  - Test Fonctionnel
  - Code Quality
  - Tests UFC
  - Release Image
  - Deploy Review
  - Stop Review
  - Deploy Staging
  - Test Staging
  - Deploy Prod
  - Test Prod

trivy-docker:
  stage: Scan docker image
  image:
    name: docker.io/aquasec/trivy:latest
    entrypoint: [""]
  variables:
    GIT_STRATEGY: none
    TRIVY_USERNAME: "$CI_REGISTRY_USER"
    TRIVY_PASSWORD: "$CI_REGISTRY_PASSWORD"
    TRIVY_AUTH_URL: "$CI_REGISTRY"
    TRIVY_NO_PROGRESS: "true"
    TRIVY_CACHE_DIR: ".trivycache/"
    FULL_IMAGE_NAME: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  script:
    - trivy --version
    - trivy image --clear-cache
    - trivy image --download-db-only
    - trivy image --exit-code 0 --format template --template "@/contrib/gitlab.tpl" --output "$CI_PROJECT_DIR/gl-container-scanning-report.json" "$FULL_IMAGE_NAME"
    - trivy image --exit-code 0 "$FULL_IMAGE_NAME"
    - trivy image --exit-code 1 --severity CRITICAL "$FULL_IMAGE_NAME"
  cache:
    paths:
      - .trivycache/
  artifacts:
    when: always
    reports:
      container_scanning: gl-container-scanning-report.json

docker-build:
  stage: Build
  script:
    - docker build -t alpinehelloworld .
    - docker save alpinehelloworld > alpinehelloworld.tar
  artifacts:
    paths:
      - alpinehelloworld.tar

tests unitaire:
  stage: Tests UFC
  script:
    - apk --no-cache add curl
    - docker rm -vf webapp || echo "Container already deleted"
    - docker rmi alpinehelloworld || echo "Image already deleted"
    - docker load < alpinehelloworld.tar
    - docker run -d --name=webapp -e PORT=5000 --expose 5000 alpinehelloworld
    - WEBAPP_IP=$(docker inspect -f '{{.NetworkSettings.IPAddress}}' webapp)
    - sleep 5
    - curl http://$WEBAPP_IP:5000/ | grep 'Hello world Toto!'
    - docker rm -vf webapp

tests fonctionnel:
  stage: Tests UFC
  script:
    - apk --no-cache add curl
    - docker rm -vf webapp || echo "Container already deleted"
    - docker rmi alpinehelloworld || echo "Image already deleted"
    - docker load < alpinehelloworld.tar
    - docker run -d --name=webapp -e PORT=5000 --expose 5000 alpinehelloworld
    - WEBAPP_IP=$(docker inspect -f '{{.NetworkSettings.IPAddress}}' webapp)
    - sleep 10
    - curl http://$WEBAPP_IP:5000/ | grep 'Hello world Toto!'
    - docker rm -vf webapp


codequality:
  image: docker:stable
  stage: Tests UFC
  script:
    - docker run --rm --env CODECLIMATE_CODE="$PWD" --volume "$PWD":/code --volume /var/run/docker.sock:/var/run/docker.sock --volume /tmp/cc:/tmp/cc codeclimate/codeclimate analyze -f html > codeclimate.html || true
  artifacts:
    paths:
      - codeclimate.html


release image:
  stage: Release Image
  variables:
    GIT_STRATEGY: none
    IMAGE_NAME_COMMIT: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
    IMAGE_NAME_SHAHASH: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
  script:
    - docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
    - docker load < alpinehelloworld.tar
    - docker tag alpinehelloworld $IMAGE_NAME_COMMIT
    - docker tag alpinehelloworld $IMAGE_NAME_SHAHASH
    - docker push $IMAGE_NAME_COMMIT
    - docker push $IMAGE_NAME_SHAHASH


deploy review:
  stage: Deploy Review
  environment:
    name: review/${CI_COMMIT_REF_NAME}
    url: http://${HOSTNAME_DEPLOY_REVIEW}
    on_stop: stop review
  only:
    - merge_requests
  script:
    - apk add openssh-client
    - eval $(ssh-agent -s)
    - mkdir -p ~/.ssh
    - chmod -R 400 ~/.ssh
    - touch ~/.ssh/known_hosts
    - cd ~/.ssh
    - echo "${SSH_KEY}" > id_rsa
    - chmod 0400 id_rsa
    - ssh-keyscan -t rsa  ${HOSTNAME_DEPLOY_REVIEW} >> ~/.ssh/known_hosts
    - command1="docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY"
    - command2="docker rm -vf webapp"
    - command3="docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"
    - command4="docker run -d -p 80:5000 -e PORT=5000 --name webapp $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"
    - ssh -t ${SSH_USER}@${HOSTNAME_DEPLOY_REVIEW} -o SendEnv=CI_REGISTRY_IMAGE -o SendEnv=CI_COMMIT_REF_NAME -o SendEnv=CI_REGISTRY_USER -o SendEnv=CI_REGISTRY_PASSWORD -o SendEnv=CI_REGISTRY -C "$command1 && $command2 && $command3 && $command4"


stop review:
  stage: Stop Review
  variables:
    GIT_STRATEGY: none
  environment:
    name: review/${CI_COMMIT_REF_NAME}
    action: stop
  only:
    - merge_requests
  when: manual
  script:
    - apk add openssh-client
    - eval $(ssh-agent -s)
    - mkdir -p ~/.ssh
    - chmod -R 400 ~/.ssh
    - touch ~/.ssh/known_hosts
    - cd ~/.ssh
    - echo "${SSH_KEY}" > id_rsa
    - chmod 0400 id_rsa
    - ssh-keyscan -t rsa  ${HOSTNAME_DEPLOY_REVIEW} >> ~/.ssh/known_hosts
    - command1="docker rm -vf webapp"
    - command2="docker rmi $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"
    - ssh -t ${SSH_USER}@${HOSTNAME_DEPLOY_REVIEW} -o SendEnv=CI_REGISTRY_IMAGE -o SendEnv=CI_COMMIT_REF_NAME -C "$command1 && $command2"

.test_template: &test
  image: alpine
  only:
    - master
  script:
    - apk --no-cache add curl
    - curl $DOMAIN | grep "Hello world Toto!"

deploy staging:
  stage: Deploy Staging
  variables:
    GIT_STRATEGY: none
  environment:
    name: gitlab-training-staging
    url: http://${HOSTNAME_DEPLOY_STAGING}
  only:
    - master
  script:
    - apk add openssh-client
    - eval $(ssh-agent -s)
    - mkdir -p ~/.ssh
    - chmod -R 400 ~/.ssh
    - touch ~/.ssh/known_hosts
    - cd ~/.ssh
    - echo "${SSH_KEY}" > id_rsa
    - chmod 0400 id_rsa
    - ssh-keyscan -t rsa  ${HOSTNAME_DEPLOY_STAGING} >> ~/.ssh/known_hosts
    - command1="docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY"
    - command2="docker rm -vf webapp"
    - command3="docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"
    - command4="docker run -d -p 80:5000 -e PORT=5000 --name webapp $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"
    - ssh -t ${SSH_USER}@${HOSTNAME_DEPLOY_STAGING} -o SendEnv=CI_REGISTRY_IMAGE -o SendEnv=CI_COMMIT_REF_NAME -o SendEnv=CI_REGISTRY_USER -o SendEnv=CI_REGISTRY_PASSWORD -o SendEnv=CI_REGISTRY -C "$command1 && $command2 && $command3 && $command4"

test staging:
  stage: Test Staging
  variables:
    GIT_STRATEGY: none
    DOMAIN: http://${HOSTNAME_DEPLOY_STAGING}
  <<: *test

deploy prod:
  stage: Deploy Prod
  environment:
    name: gitlab-training-prod
    url: http://${HOSTNAME_DEPLOY_PROD}
  only:
    - master
  variables:
    GIT_STRATEGY: none
  script:
    - apk add openssh-client
    - eval $(ssh-agent -s)
    - mkdir -p ~/.ssh
    - chmod -R 400 ~/.ssh
    - touch ~/.ssh/known_hosts
    - cd ~/.ssh
    - echo "${SSH_KEY}" > id_rsa
    - chmod 0400 id_rsa
    - ssh-keyscan -t rsa  ${HOSTNAME_DEPLOY_PROD} >> ~/.ssh/known_hosts
    - command1="docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY"
    - command2="docker rm -vf webapp"
    - command3="docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"
    - command4="docker run -d -p 80:5000 -e PORT=5000 --name webapp $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"
    - ssh -t ${SSH_USER}@${HOSTNAME_DEPLOY_PROD} -o SendEnv=CI_REGISTRY_IMAGE -o SendEnv=CI_COMMIT_REF_NAME -o SendEnv=CI_REGISTRY_USER -o SendEnv=CI_REGISTRY_PASSWORD -o SendEnv=CI_REGISTRY -C "$command1 && $command2 && $command3 && $command4"

test prod:
  stage: Test Prod
  variables:
    GIT_STRATEGY: none
    DOMAIN: http://${HOSTNAME_DEPLOY_PROD}
  <<: *test
